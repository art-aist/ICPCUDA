/*
 * ICPOdometry.cpp
 *
 *  Created on: 17 Sep 2012
 *      Author: thomas
 */
#include "ICPOdometry.h"
#include <Eigen/Eigen>

namespace TU
{
namespace cu
{
/************************************************************************
*  class PointPlaneMetric<T>							*
************************************************************************/
template <class T>
class PointPlaneMetric
{
  public:
    using value_type		= T;
    using transform_type	= Rigidity<value_type, 3>;
    using point_type		= typename transform_type::point_type;
    using direction_type	= typename transform_type::direction_type;
    using moment_type		= array<value_type, 29>;
    using deviation_type	= array<value_type, 6>;
    using moment_matrix_type	= Eigen::Matrix<value_type, 6, 6>;
    using deviation_vector_type	= Eigen::Matrix<value_type, 6, 1>;

  public:
    static moment_matrix_type
		A(const moment_type& moment)
		{
		    auto		p = moment.data();
		    moment_matrix_type	m;
		    for (int i = 0; i < m.rows(); ++i)
		    {
			for (int j = i; j < m.cols(); ++j)
			    m(j, i) = m(i, j) = *p++;
			++p;
		    }

		    return m;
		}
    static deviation_vector_type
		b(const moment_type& moment)
		{
		    deviation_vector_type	v;
		    v << moment[6],  moment[12], moment[17],
			 moment[21], moment[24], moment[26];

		    return v;
		}
    static value_type
		npoints(const moment_type& moment)
		{
		    return moment[28];
		}
    static value_type
		mse(const moment_type& moment)
		{
		    return moment[27] / moment[28];
		}
    
    PointPlaneMetric(const transform_type& T_pc, const Intrinsics<T>& intr,
	   const PtrStep<point_type>& xc, const PtrStep<direction_type>& nc,
	   const PtrStep<point_type>& xp, const PtrStep<direction_type>& np,
	   value_type sqdist_thresh, value_type sqangle_thresh,
	   int cols, int rows)
	:_T_pc(T_pc), _intr(intr), _xc(xc), _nc(nc), _xp(xp), _np(np),
	 _sqdist_thresh(sqdist_thresh), _sqangle_thresh(sqangle_thresh),
	 _cols(cols), _rows(rows)
    {
    }

    __device__ __forceinline__ array<value_type, 29>
    operator()(int i) const
    {
	const int		v    = i / _cols;
	const int		u    = i - (v * _cols);
	const point_type	xc   = _xc.ptr(v)[u];
	const auto		xc_p = _T_pc(xc);
	const auto		uv_p = _intr(xc_p);
	const int		up   = __float2int_rn(uv_p.x);
	const int		vp   = __float2int_rn(uv_p.y);

	if (0 <= up && up < _cols && 0 <= vp && vp < _rows &&
	    xc.z > 0 && xc_p.z > 0)
	{
	    const point_type		xp   = _xp.ptr(vp)[up];
	    const direction_type	np   = _np.ptr(vp)[up];
	    const direction_type	nc   = _nc.ptr(v)[u];
	    const direction_type	nc_p = _T_pc.direction(nc);

	    if (square(cross(nc_p, np)) < _sqangle_thresh &&
		square(xp - xc_p)	< _sqdist_thresh &&
		!isnan(nc.x) && !isnan(np.x))
	    {
		array<value_type, 7>	row;
		row[0] = np.x;
		row[1] = np.y;
		row[2] = np.z;
		const auto	x_cross_n = cross(xc_p, np);
		row[3] = x_cross_n.x;
		row[4] = x_cross_n.y;
		row[5] = x_cross_n.z;
		row[6] = dot(np, xp - xc_p);

		auto	m = row.template ext<28+1>();
		m[28] = 1;

		return m;
	    }
	}

	return {0};
    }

  private:
    const transform_type		_T_pc;
    const Intrinsics<value_type>	_intr;

    const PtrStep<point_type>		_xc;
    const PtrStep<direction_type>	_nc;

    const PtrStep<point_type>		_xp;
    const PtrStep<direction_type>	_np;

    const value_type			_sqdist_thresh;
    const value_type			_sqangle_thresh;

    const int				_cols;
    const int				_rows;
};

/************************************************************************
*  class ICPOdometry							*
************************************************************************/
ICPOdometry::ICPOdometry(value_type dist_thresh, value_type angle_thresh)
    :_sqdist_thresh(0),
     _sqangle_thresh(0),
     _iterations{10, 5, 4},
     _pyramidA(),
     _pyramidB(),
     _pyramid_p(&_pyramidB),
     _pyramid_c(&_pyramidA),
     _depth_tmp(),
     _depth_tmp_f(),
     _tmp(),
     _moment(),
     _num_inliers(0),
     _rmse(0)
{
    set_dist_thresh(dist_thresh);
    set_angle_thresh(angle_thresh);
    _moment.create(1);
}

ICPOdometry::~ICPOdometry()
{
}

ICPOdometry::transform_type
ICPOdometry::get_incremental_transform()
{
    transform_type	T_pc;
    T_pc.initialize();
    
    for (int level = _iterations.size(); --level >= 0; )
    {
      //std::cerr << "=== level= " << level << " ===" << std::endl;
	
	for (int n = 0; n < _iterations[level]; ++n)
	{
	    value_type	residual_inliers[2];

	    update_transform(T_pc,
			     current_frame(level), previous_frame(level));

	    // std::cerr << "  n=" << n << ", #inliers=" << _num_inliers
	    // 	      << ", rmse=" << _rmse << std::endl;
	}
    }

    swap_pyramids();

    return T_pc;
}

void
ICPOdometry::update_transform(transform_type& T_pc,
			      const Frame& frame_c, const Frame& frame_p)
{
    using metric_type	= PointPlaneMetric<value_type>;
    
    const int	cols = frame_c.points.cols();
    const int	rows = frame_c.points.rows();

    if (frame_p.points.cols() != cols || frame_p.points.rows() != rows)
	return;
    
    metric_type	metric(T_pc, frame_p.intr,
		       frame_c.points, frame_c.normals,
		       frame_p.points, frame_p.normals,
		       _sqdist_thresh, _sqangle_thresh, cols, rows);

    size_t	tmp_size = 0;
    cub::DeviceReduce::Sum(nullptr, tmp_size,
			   thrust::make_transform_iterator(
			       thrust::make_counting_iterator(0), metric),
			   _moment.ptr(), cols * rows);
    if (tmp_size > _tmp.size())
	_tmp.resize(tmp_size);

  // Perform parallel reduction of moments for current points.
    cub::DeviceReduce::Sum(_tmp.data().get(), tmp_size,
			   thrust::make_transform_iterator(
			       thrust::make_counting_iterator(0), metric),
			   _moment.ptr(), cols * rows);
    typename metric_type::moment_type	moment;
    _moment.download(&moment);
    
    cudaSafeCall(cudaGetLastError());

    const auto	A = metric_type::A(moment);
    const auto	b = metric_type::b(moment);
    const auto	update = A.ldlt().solve(b).eval();
    T_pc = transform_type::exp(update.data()) * T_pc;

    _num_inliers = metric_type::npoints(moment);
    _rmse	 = std::sqrt(metric_type::mse(moment));
}

std::array<DeviceArray2D<uint16_t>, ICPOdometry::NUM_PYRS>&
ICPOdometry::upload(const uint16_t* depth)
{
    _depth_tmp[0].upload(depth, sizeof(uint16_t)*width(), height(), width());
    return _depth_tmp;
}

std::array<DeviceArray2D<float>, ICPOdometry::NUM_PYRS>&
ICPOdometry::upload(const float* depth)
{
    _depth_tmp_f[0].upload(depth, sizeof(float)*width(), height(), width());
    return _depth_tmp_f;
}

}	// namespace cu
}	// namespace TU
