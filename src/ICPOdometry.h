/*
 * ICPOdometry.h
 *
 *  Created on: 17 Sep 2012
 *      Author: thomas
 */
#pragma once

#include "internal.h"

#include <array>
#include <thrust/device_vector.h>
#include <TU/cu/array.h>
#include "TU/cu/vec.h"

namespace TU
{
namespace cu
{
/************************************************************************
*  class ICPOdometry							*
************************************************************************/
class ICPOdometry
{
  public:
    using value_type		= float;
    using transform_type	= Rigidity<value_type, 3>;
    using point_type		= transform_type::point_type;
    using normal_type		= transform_type::direction_type;

  private:
    struct Frame
    {
	Intrinsics<value_type>		intr;
	DeviceArray2D<point_type>	points;
	DeviceArray2D<normal_type>	normals;
    };

    constexpr static int	NUM_PYRS = 3;

    using Pyramid		= std::array<Frame, NUM_PYRS>;

  public:
		ICPOdometry(value_type dist_thresh=0.10,
			    value_type angle_thresh=20.0);
    virtual	~ICPOdometry();

    template <class ITER_K, class ITER_D>
    bool	initialize(int w, int h, ITER_K K, ITER_D D)		;
    
    value_type	get_dist_thresh() const
		{
		    return std::sqrt(_sqdist_thresh);
		}
    value_type	set_dist_thresh(value_type dist_thresh)
		{
		    _sqdist_thresh = dist_thresh * dist_thresh;
		    return get_dist_thresh();
		}
    value_type	get_angle_thresh() const
		{
		    return std::asin(std::sqrt(_sqangle_thresh)) * 180.0/M_PI;
		}
    value_type	set_angle_thresh(value_type angle_thresh)
		{
		    _sqangle_thresh
			= TU::square(std::sin(angle_thresh * M_PI/180.0));
		    return get_angle_thresh();
		}

    int		width()	 const	{ return current_frame(0).points.cols(); }
    int		height() const	{ return current_frame(0).points.rows(); }
    
    template <class T, class ITER_K, class ITER_D>
    void	input_frame(const T *depth, int w, int h,
			    ITER_K K, ITER_D D, value_type depthCutoff=20.0f);

    transform_type
		get_incremental_transform()				;

    value_type	rsme()		const	{ return _rmse; }
    int		num_inliers()	const	{ return _num_inliers; }

  private:
    void		update_transform(transform_type &T_pc,
					 const Frame& frame_c,
					 const Frame& frame_p)		;
    void		swap_pyramids()
			{
			    std::swap(_pyramid_p, _pyramid_c);
			}
    const Frame&	current_frame(size_t level) const
			{
			    return (*_pyramid_c)[level];
			}
    Frame&		current_frame(size_t level)
			{
			    return (*_pyramid_c)[level];
			}
    const Frame&	previous_frame(size_t level) const
			{
			    return (*_pyramid_p)[level];
			}
    
    std::array<DeviceArray2D<uint16_t>, NUM_PYRS>&
			upload(const uint16_t* depth)			;
    std::array<DeviceArray2D<float>, NUM_PYRS>&
			upload(const float* depth)			;
    
  private:
    value_type						_sqdist_thresh;
    value_type						_sqangle_thresh;

    std::array<int, NUM_PYRS>				_iterations;
    Pyramid						_pyramidA;
    Pyramid						_pyramidB;
    Pyramid*						_pyramid_p;
    Pyramid*						_pyramid_c;

  // Temporary buffers
    std::array<DeviceArray2D<uint16_t>, NUM_PYRS>	_depth_tmp;
    std::array<DeviceArray2D<float>,	NUM_PYRS>	_depth_tmp_f;
    thrust::device_vector<uint8_t>			_tmp;	// for CUB
    DeviceArray<array<value_type, 29> >			_moment;

    int							_num_inliers;
    value_type						_rmse;
};

template <class T, class ITER_K, class ITER_D> void
ICPOdometry::input_frame(const T *depth, int w, int h,
			 ITER_K K, ITER_D D, value_type depthCutoff)
{
    if (w != width() && h != height())
    {
	_num_inliers = w * h;
	_rmse	     = 0;

	for (int level = 0; level < NUM_PYRS; ++level)
	{
	    const int	pyr_rows = h >> level;
	    const int	pyr_cols = w >> level;
	    auto&	frame = current_frame(level);
	    
	    frame.intr.initialize(K, D, 1.0/value_type(1 << level));
	    frame.points .create(pyr_rows, pyr_cols);
	    frame.normals.create(pyr_rows, pyr_cols);

	    _depth_tmp[level]  .create(pyr_rows, pyr_cols);
	    _depth_tmp_f[level].create(pyr_rows, pyr_cols);
	}
    }

    auto&	depth_tmp = upload(depth);

    for (int level = 1; level < NUM_PYRS; ++level)
	pyramid_down(depth_tmp[level - 1], depth_tmp[level]);

    for (int level = 0; level < NUM_PYRS; ++level)
    {
	auto&	frame = current_frame(level);
	create_points(frame.intr, depth_tmp[level], frame.points, depthCutoff);
	create_normals(frame.points, frame.normals);
    }

    cudaDeviceSynchronize();
}

}	// namespace cu
}	// namespace TU
