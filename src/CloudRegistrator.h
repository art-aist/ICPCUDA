// Software License Agreement (BSD License)
//
// Copyright (c) 2021, National Institute of Advanced Industrial Science and Technology (AIST)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//  * Neither the name of National Institute of Advanced Industrial
//    Science and Technology (AIST) nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: Toshio Ueshiba
//
/*!
  \file		Registrator.h
  \author	Toshio Ueshiba
*/
#pragma once

#include <fstream>
#include <image_transport/image_transport.h>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>
#include <aist_utility/sensor_msgs.h>
#include <TU/iterator.h>
#include <TU/Image++.h>
#include "ICPOdometry.h"

namespace icpcuda_ros
{
/************************************************************************
*  class CloudRegistrator						*
************************************************************************/
class CloudRegistrator
{
  private:
    using camera_info_t		= sensor_msgs::CameraInfo;
    using camera_info_cp	= sensor_msgs::CameraInfoConstPtr;
    using image_t		= sensor_msgs::Image;
    using image_cp		= sensor_msgs::ImageConstPtr;
    using cloud_t		= sensor_msgs::PointCloud2;
    using cloud_cp		= sensor_msgs::PointCloud2ConstPtr;
    using rgb_type		= TU::RGB;
    using registrator_type	= TU::cu::ICPOdometry;
    using transform_type	= registrator_type::transform_type;

    struct ddynamic_reconfigure_t : ddynamic_reconfigure::DDynamicReconfigure
    {
	using super	= ddynamic_reconfigure::DDynamicReconfigure;

	ddynamic_reconfigure_t(const ros::NodeHandle& nh) :super(nh)	{}

	void	publishServicesTopics()
		{
		    super::publishServicesTopics();
		    super::updateConfigData(generateConfig());
		}
    };

    using cloud_buffer_type	= std::array<cloud_t, 10>;
    using cloud_iterator	= TU::ring_iterator<typename
					cloud_buffer_type::iterator>;
    using const_cloud_iterator	= TU::ring_iterator<typename
					cloud_buffer_type::const_iterator>;

  public:
		CloudRegistrator(const ros::NodeHandle& nh)		;

    void	run()							;

  private:
    template <class T, class U>
    void	set_registrator_param(U (registrator_type::* setter)(U),
				      T value, const std::string& name)	;

    void	camera_cb(const image_cp&	depth,
			  const camera_info_cp&	camera_info)		;
    void	outputFreiburg(double timestamp)			;

  private:
    ros::NodeHandle			_nh;

  // input/output stuffs
    image_transport::ImageTransport	_it;
    image_transport::CameraSubscriber	_camera_sub;
    const ros::Publisher		_cloud_pub;

    ddynamic_reconfigure_t		_ddr;

    registrator_type			_odometry;
    transform_type			_T_wc;
    std::ofstream			_out;

    cloud_buffer_type			_clouds;
    cloud_iterator			_current_cloud;
    cloud_t				_registered_cloud;

    constexpr static std::array<rgb_type, 16>
					_cmap{{{255,   0,   0},	//  0: red
					       {  0, 255,   0},	//  1: green
					       {  0,   0, 255},	//  2: blue
					       {  0, 255, 255},	//  3: cyan
					       {255,   0, 255},	//  4: magenta
					       {255, 255,   0},	//  5: yellow
					       {100,  20,  50},	//  6:
					       {  0,  30, 255},	//  7:
					       { 10, 255,  60},	//  8:
					       { 80,  10, 100},	//  9:
					       {  0, 255, 200},	// 10:
					       { 10,  60,  60},	// 11:
					       {255,   0, 128},	// 12:
					       { 60, 128, 128},	// 13:
					       {  0,   0,   0},	// 14: black
					       {255, 255, 255}}	// 15: white
					};
};

}	// namespace cuda_cloud_registrator
